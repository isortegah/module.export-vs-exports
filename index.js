

/**
*	Ejemplo 1:
*/
var a = require('./lib/ejemplo1');
console.log(a);

/**
* Ejemplo 2:
*/

require('./lib/ejemplo2');
ejemplo2();

/**
* Ejemplo 3:
*/
console.log(require('./lib/ejemplo3'));

/**
* Ejemplo 4:
*/
console.log(require('./lib/ejemplo4'));

/**
* Ejemplo 5:
*/ 

console.log(require('./lib/ejemplo5'));

/**
* Ejemplo 6:
*/
var foo = require('./lib/ejemplo6.js');
console.log(foo(5));

/**
* Ejemplo 7:
*/
var Saludo = require('./lib/ejemplo7.js');
var s = new Saludo('Miguel', 45);
s.acercade();

/**
* Ejemplo 8:
*/
var u = require('./lib/ejemplo8'); 
var test = new u([0,3,4,5,4,3,4,2]);
 
console.log(test.min()); 
console.log(test.max()); 
console.log(test.unique());