module.exports VS exports
==============================
      Utilize los Tags para ver la evolución de los ejemplos aqui presentados.
La función require() regresa el contenido de la propiedad "module.exports" además de procesar el contenido del archivo JS que se le pasa como ruta (path de archivo).
***
####Tag Paso 1
Comenzaremos creando un módulo simple sin el uso de module.exports. Vea el contenido del archivo ejemplo1 en el cual puede ver el siguiente contenido:

        console.log('Hello World');
En el archivo **index.js** declaramos el **require** que ejecutara el contenido de **Ejemplo1**.
        
        require('./lib/ejemplo1');
Al ejecutar el archivo **index.js** veremos el siguiente mensaje en la consola:

```sh
    $ Hello World
```
***
####Tag Paso 2
Ahora realice los siguientes cambios en el archivo ***index.js***:

        var a = require('./lib/ejemplo1');
        console.log(a);
Lo que se realizo fue asignar el contenido devuelto por el ***require()*** a la variable ***a*** e imprimir en consola el contenido de esta última, lo que nos arroja en la línea de comandos:
```sh
> Hello World
> {}
```
Como resultado obtuvimos la ejecución de ***Ejemplo1*** y un objeto vacío guardado en la variable ***a*** esto debido a que el ***module.exports*** no contiene ningún valor, y su valor Default es ***{}***. 

Ahora pasemos el ***Ejemplo2*** en dicho archivo se puede encontrar el siguiente código:

        ejemplo2 = function () {
        	console.log('foo!');
        }
Como se puede ver, se está declarando una función y asignando a la variable ***ejemplo2***

En el archivo ***index.js*** nos encontramos con el siguiente cambio:

        require('./lib/ejemplo2');
        ejemplo2();
Se está realizando el ***require*** y llamamos la función que se declaró dentro del archivo ***ejemplo2***. En la terminal al ejecutar el archivo ***index.js*** obtenemos el siguiente resultado:
```sh
> foo!
```
Ahora verifique cual es el resultado de realizar el siguiente cambio en el archivo ***index.js***:

        var b = require('./lib/ejemplo2');
        console.log(b);
***
####Tag Paso 3
Ya que se ha comprendido el uso de ***require*** pasemos a lo que nos interesa, el uso de ***module.exports*** y ***exports***. Inicialmente estos dos elementos apuntan al mismo objeto vacio.
![Alt text](./imgs/export.png "Modelo de module.exports")

Podemos añadir propiedades a este objeto ya sea utilizando ***module.exports*** o ***exports***, ya que ambos apuntan al mismo objeto, no importa cual usemos. Para ver este caso vea el contenido del archivo ***ejemplo3***, en el cual se encuentra el siguiente código:

        exports.foo = "Desde exports!!!";
        module.exports.baz = "Desde module.exports!!!";

Se está agregando al elemento ***exports*** el elemento ***foo*** y a ***module.exports*** el elemento ***baz*** y a cada uno de estos un String. Ahora veamos en el archivo ***index.js*** que estamos imprimiendo en la línea de comandos lo que nos esta retornando ***require*** y nos muestra lo siguiente:

        { 
            foo: 'Desde exports!!!', 
            baz: 'Desde module.exports!!!' 
        }
Pero, ¿qué pasa si desea exportar una función, o una cadena o una clase?. Esto es cuando la diferencia entre ***exports*** y ***module.exports*** es importante.

Debemos tener muy presente lo siguiente:
####module.exports Wins!!!

Lo que esto significa es que cualquier objeto que se asigne a ***module.exports*** será el objeto que se exporte al usar ***require***..

Si se desea exportar una función del módulo y lo ha asignado a ***exports*** y no a *** module.exports*** entonces esto sucede:

![Alt text](./imgs/export2.png "Modelo de module.exports")
***
#### Tag Paso 4
Veamos ahora el archivo ***ejemplo4***, que nos ayudara a ejemplificar lo anterior. Encontraremos el siguiente código:

        exports = function (){
            console.log("Desde exports!!!");
        }
En el archivo ***index.js*** vemos el siguiente cambio:

        console.log(require('./lib/ejemplo4'));
Al ejecutar el ***index.js*** en la línea de comandos veremos lo siguiente:

        {}
Si recordamos, mencionamos que ***require*** regresa el contenido de ***module.exports*** el cual es un objeto vacío en este ejemplo.

Realice la siguiente modificación al archivo ***ejemplo4***:

        exports.a = function (){
            console.log("Desde exports!!!");
        }
Como se ve se esta agregando el elemento ***a*** a exports y para este caso es como si estuviéramos realizando la siguiente codificación:
        
        a = function (){
            console.log("Desde exports!!!");
        }
        module.exports.a;
O de otra forma más corta:

        module.exports.a = function() {
            console.log("Desde exports!!!");
        }
Al ver la ejecución del ***index.js***, en la línea de comandos nos mostrara lo siguiente:

        { a: [Function] }
En el caso de que se quiera usar ***exports*** para exponer los recursos del módulo se tendría que realizar la siguiente declaración:
    
        exports = module.exports = function (){
        console.log("module.exports!!!");
        }

        exports.foo = "bar";
El anterior código lo puede visualizar en el archivo ***ejemplo5***. En el archivo ***index.js*** se agrego lo siguiente:

        console.log(require('./lib/ejemplo5'));
Al ejecutarlo en la línea de comandos veremos lo siguiente:

        { [Function] foo: 'bar' }

***
####Tag Paso 5
La mayor parte del tiempo, se requiere exportar una sola función o constructor con module.exports porque por lo general es mejor para un módulo realizar una cosa.

La característica de ***exports*** fue originalmente la principal forma de exportar la funcionalidad y ***module.exports*** fue una ocurrencia tardía, pero ***module.exports*** demostró ser mucho más útil en la práctica al ser más directa, clara, y evitar la duplicación.

En los primeros días, este estilo solía ser mucho más común:

        exports.foo = function (n) { return n * 111 }
Y se ejecutaba de la siguiente forma:

        var foo = require('./lib/ejemplo6.js');
        console.log(foo.foo(5));
        
Notese que la declaración  ***foo.foo*** es un poco superfluo. Usando module.exports se hace más claro (Vea el archivo ***ejemplo6***):

        module.exports = function (n) { return n * 111 };
Y se ejecutaria de la siguiente forma:

        var foo = require('./lib/ejemplo6.js');
        console.log(foo(5));
##Conclución hasta este punto
Para asegurarse de que el contenido de un módulo que se requiera exponer para ser utilizado dentro de otro archivo, debemos constatar que se este asignando al elemento ***exports*** dentro de ***module***. Podemos entenderlo como sigue:

        var module = {
            exports : {...}
        }
        return module;
Algunos ejemplos de exposición serían los siguientes:

        exports = function fn(){}; // salida "@ruta {}"
        exports.fn = function fn(){};  // salida "@ruta { fn: [Function: fn] }"
        module.exports = function fn(){};  // salida "@ruta function fn(){}"
        module.exports.fn = function fn(){};  // salida "@ruta { fn: [Function: fn] }"
***
####Tag Paso 6
##Módulo como clase
Veamos dos formas de manejar un módulo parecido a una clase de ***JAVA***. La primera forma es la siguiente. En el archivo ***ejemplo7*** encontramos la siguiente declaración:

        module.exports = function(nombre, edad) {
            this.nombre = nombre;
            this.edad = edad;
            this.acercade = function() {
                console.log(this.nombre +' tiene '+ this.age +' años de edad!!!');
            };
        };
Para ejecutar este módulo lo haremos de la siguiente forma:

        var Saludo = require('./lib/ejemplo7.js');
        var s = new Saludo('Miguel', 45);
        s.acercade();
Lo anterior nos arroja en la línea de comandos:

        Miguel tiene 45 años de edad!!!
Una segunda forma que nos acerca más a la forma de usar las clases en ***JAVA*** es la siguiente. En el archivo ***ejemplo8*** tenemos la siguiente declaración:

        var ut = function(array){
          this.array = array;
        }
        ut.prototype.max = function(){  
          var a = this.array.sort();
          return a[a.length-1];
        };
        ut.prototype.min = function(){  
          var a = this.array.sort();
          return a[0];
        };
        ut.prototype.unique = function(){  
          return this.array.filter(function(v, i, a) { 
                              return a.lastIndexOf(v) === i; })
                            .sort();
        };
         
        module.exports = exports = ut;
Nótese  que se está utilizando la palabra reservada ***prototype*** para agregar los métodos a la variable ***ut*** y posteriormente llamaremos al módulo de la siguiente forma:

        var u = require('./lib/ejemplo8'); 
        var test = new u([0,3,4,5,4,3,4,2]);
         
        console.log(test.min()); 
        console.log(test.max()); 
        console.log(test.unique());
La salida en la línea de comandos será como sigue:

        0
        5
        [ 0, 2, 3, 4, 5 ]
Solo queda decir que podemos asignar cualquier cantidad de elementos en ***module.exports*** ya sea una función, una serie de funciones asignadas a distintos tipos de elementos dentro del objeto devuelto, arreglos de elementos y una combinación muy variada de las mismas.

###Felices desarrollos!!!


